<?php
namespace UrlIterator;

/**
 * Class iterator
 */
class Iterator implements \IteratorAggregate 
{
    /**
     * $url
     *
     * @var string
     */
    private $url;

    /**
     * @param string $url
     */
    public function __construct($url){
        $this->url = $url;
    }

    /**
     * @return void
     */
    public function getIterator(){

        $options = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];  

        $stream = fopen($this->url, 'r', false, stream_context_create($options));

        if (!$stream) {
            throw new \Exception();
        }

        while($char = fgets($stream)){
            yield $char;
        }

        fclose($stream);
    }
}