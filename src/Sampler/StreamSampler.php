<?php

namespace Sampler;

/**
 * Class StreamSampler
 */
class StreamSampler {

    /**
     * @var \Traversable
     */
    private $iterator;

    /**
     * @param \Traversable $iterator
     */
    public function __construct(\Traversable $iterator){
        $this->iterator = $iterator;
    }

    /**
     * @param int $length
     * @return array
     */
    public function fetch($length){
        $sample = [];
        $i = 0;

        foreach($this->iterator as $item){
            if($i < $length){
                $sample[$i] = $item;
            }else{
                $random = (int)mt_rand(0, $i);
                if ($random < $length) {
                    $sample[$random] = $item;
                }
            }

            $i++;
        }

        return $sample;
    }


}