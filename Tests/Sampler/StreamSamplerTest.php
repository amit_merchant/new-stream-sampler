<?php

namespace Tests\Sampler;

use Sampler\StreamSampler;
use PHPUnit\Framework\TestCase;

class StreamIteratorTest extends TestCase
{
    private $sampler;

    protected function setUp(){
        $this->sampler = new StreamSampler(new \ArrayIterator(['a','b','c','d','e']));
    }

    public function testSampler(){
        $this->assertCount(5, $this->sampler->fetch(5));
    }
}